const fs = require('fs');
const path = require('path');
const products_data = require('../data/products.json');

const getProducts = (query, id, data) => {

  if(query === 'all') {
    return products_data;
  }

  if(query === 'reviews') {
    return products_data.filter((product) => {
      if (product.id === id) {
        return product.review;
      }
    });
  }

  if(query === 'id') {
    return products_data.filter((product) => {
      return product.id === id;
    });
  }

  if(query === 'post') {
    console.log('POSTED  req.body ',query, id, data);

    let postData = {
      id,
      ...data
    };

    let dataToWrite = [
      ...products_data,
      postData
    ];

    fs.writeFile(path.join(__dirname + '/../data/products.json'), JSON.stringify(dataToWrite), 'utf8', (err) => {
      if(err) {
        return console.log(err);
      }
    });
  }
};

module.exports = getProducts;