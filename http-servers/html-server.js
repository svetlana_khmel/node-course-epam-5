const http = require('http');
const fs = require('fs');
const path = require('path');

const server = http.createServer((req, res) => {

  const readFile = new Promise((resolve, reject) => {
    const file = fs.readFileSync(path.join(__dirname, 'public/index.html'), 'UTF-8');
    resolve(file);
  });

  readFile.then((data) => {
    res.writeHead(200, {'Content-Type':'text/html'});
    res.end(req.url,data);
  });

});

server.listen(3000);
