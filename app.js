const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const products = require('./routes/products');
const user = require('./routes/users');
const auth = require('./routes/auth');
const passportLocal = require('./routes/passportLocal');
const passportFacebook = require('./routes/passportFacebook');
const passport = require('passport');

app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(passport.initialize())
app.use(passport.session());

app.use(function (err, req, res, next) {
  console.error(err.stack)
  res.status(500).send('Something broke!')
})

app.use('/api/products', products);
app.use('/api/users', user);
app.use('/auth', auth);
app.use('/local', passportLocal);
app.use('/auth/facebook', passportFacebook);

export default app;
