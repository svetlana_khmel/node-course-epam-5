const parsedCookies = (req, res, next) => {
  req.parsedCookies = req.cookies;
  return req;
  next();
}

module.exports = parsedCookies;