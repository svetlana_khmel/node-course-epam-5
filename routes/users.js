const express = require('express');
const router = express.Router();
const getUsers = require('../controllers/users');
const checkToken = require('./checkToken');

router.get('/', checkToken, (req, res) => {
  console.log('333 ', req);
  res.json(getUsers());
});

module.exports = router;
