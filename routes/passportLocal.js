const express = require('express');
const router = express.Router();
const passport = require('passport')
  , LocalStrategy = require('passport-local').Strategy;

passport.serializeUser(function(username, done) {
  console.log('SerializeUser    ', username);
  done(null, username);
});

passport.deserializeUser(function(id, done) {
  // User.findById(id, function(err, user) {
  //   done(err, user);
  // });
});

passport.use(new LocalStrategy(
  function(username, password, done) {
     // if (err) { return done(err); }
      if (username != 'test') {
        return done(null, false, { message: 'Incorrect username.' });
      }
      if (password != 'test') {
        return done(null, false, { message: 'Incorrect password.' });
      }
      return done(null, username);
  }
));

router.post('/',
  passport.authenticate('local', { successRedirect: '/',
    failureRedirect: '/login',
    failureFlash: false })
);

module.exports = router;

